#include "space.hh"
#include "nabla.hh"
#include "navier_stokes.hh"

using namespace std;

void space::step(double dt)
{
	for (size_t i = 1; i < its.size() - 1; ++i) {
	for (size_t j = 1; j < its[i].size() - 1; ++j) {
		item* c = its[i][j];
		if (c->isbox)
			compute_nabla(c->get_data(), its[i-1][j]->get_data(c), its[i+1][j]->get_data(c), its[i][j+1]->get_data(c), its[i][j-1]->get_data(c), h);
	}}

	for (size_t i = 1; i < its.size() - 1; ++i) {
	for (size_t j = 1; j < its[i].size() - 1; ++j) {
		item* c = its[i][j];
		if (c->isbox)
			compute_navier_stokes(c->get_data(), its[i-1][j]->get_data(c), its[i+1][j]->get_data(c), its[i][j+1]->get_data(c), its[i][j-1]->get_data(c), h, g, rho, n1, n2);
	}}

	for (size_t i = 1; i < its.size() - 1; ++i) {
	for (size_t j = 1; j < its[i].size() - 1; ++j) {
		item* c = its[i][j];
		if (c->isbox) {
			c->d.v += c->d.dvdt * dt;
			c->d.p += c->d.dpdt * dt;
		}
	}}
}

