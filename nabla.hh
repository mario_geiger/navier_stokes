#ifndef NABLA_HH
#define NABLA_HH

#include "data.hh"

inline void compute_nabla(data* c, data* l, data* r, data* t, data* b, double h)
{
	// calcules des dérivées de 1er ordre
	c->grad_p.x = (r->p - l->p) / (2 * h);
	c->grad_p.y = (t->p - b->p) / (2 * h);
	
	c->dvdx = (r->v - l->v) / (2 * h);
	c->dvdy = (t->v - b->v) / (2 * h);
	
	c->d2vdx2 = (r->v - 2.0 * c->v + l->v) / (h * h);
	c->d2vdy2 = (t->v - 2.0 * c->v + b->v) / (h * h);
}

#endif


