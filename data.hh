#ifndef DATA_HH
#define DATA_HH

#include "vec2.hh"

struct data
{
	double p, dpdt; // pressure
	vec2 grad_p;
	vec2 v, dvdt, dvdx, dvdy, d2vdx2, d2vdy2;
};

#endif


