/*
# = wall
v = flux
h = hole

Exemple:
#####vv#
#      #
#  h   #
#      #
########

*/

#ifndef SPACE_HH
#define SPACE_HH

#include "item.hh"
#include <vector>

struct space
{
	std::vector<std::vector<item*>> its;
	double n1, n2;
	double rho;
	double h;
	vec2 g;
	
	void step(double dt); // lance un tas de getdata et de compute. puis euler.
};

#endif


