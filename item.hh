/*
une case virtuelle (bord oubien pas)
*/

#ifndef ITEM_HH
#define ITEM_HH

#include "data.hh"

struct item
{
	double x, y;
	data d;
	bool isbox;
	item()
	{
		isbox = false;
	}
	virtual data* get_data(item* receiver = nullptr);
};

struct box : item
{
	box()
	{
		isbox = true;
	}
	
	data* get_data(item*)
	{
		return &d;
	}
};

struct wall : item
{
	data* get_data(item* receiver)
	{
		d = *receiver->get_data();
		d.v = d.dvdx = d.dvdy = d.d2vdx2 = d.d2vdy2 = vec2(0.0, 0.0);
		return &d;
	}
};

struct hole : item
{
	double pressure;
	data* get_data(item* receiver)
	{
		d = *receiver->get_data();
		d.p = pressure;
		return &d;
	}
};

struct wind : item
{
	data* get_data(item*)
	{
		return &d;
	}
};

#endif


