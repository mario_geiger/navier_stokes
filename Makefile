CXX = clang
CC = clang

CXXFLAGS = -O3 -Werror -Wall -W -g -std=c++11
LDLIBS   = -O3 -Werror -Wall -W -g -lstdc++ -lm

all: main

main: main.o space.o

clean:
	rm -f *.o

