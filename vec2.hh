#ifndef VEC2_HH
#define VEC2_HH

struct vec2
{
	double x, y;

	vec2(double _x, double _y);
	vec2();
	vec2(const vec2& other);

	vec2& operator+=(const vec2& other);
	friend vec2 operator+(vec2 vect1, const vec2& vect2);
	vec2 operator-() const;
	vec2& operator-=(const vec2& other);
	friend vec2 operator-(vec2 vect1, const vec2& vect2);
	vec2& operator*=(double nb);
	friend vec2 operator*(vec2 vect, double nb);
	friend vec2 operator*(double nb, vec2 vect);
	vec2& operator/=(double nb);
	friend vec2 operator/(vec2 v, double nb);
};

inline vec2::vec2(double _x, double _y) : x(_x), y(_y) {}

inline vec2::vec2() : vec2(0.0, 0.0) {}

inline vec2::vec2(const vec2& other) : vec2(other.x, other.y) {}

inline vec2& vec2::operator+=(const vec2& other) {
	x += other.x;
	y += other.y;
	return *this;
}

inline vec2 operator+(vec2 vect1, const vec2& vect2) { return vect1 += vect2; }

inline vec2 vec2::operator-() const { return vec2(-x,-y); }

inline vec2& vec2::operator-=(const vec2& other) {
	x -= other.x;
	y -= other.y;
	return *this;
}

inline vec2 operator-(vec2 vect1, const vec2& vect2) { return vect1 -= vect2; }

inline vec2& vec2::operator*=(double nb) { 
	x *= nb;
	y *= nb;
	return *this;
}

inline vec2 operator*(vec2 vect, double nb) {
	return vect *= nb;
}

inline vec2 operator*(double nb, vec2 vect) {
	return vect * nb;
}

inline vec2& vec2::operator/=(double nb) {
	x /= nb;
	y /= nb;
	return *this;
}

inline vec2 operator/(vec2 v, double nb) {
	return v /= nb;
}

#endif


