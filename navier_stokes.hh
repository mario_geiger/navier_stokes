#ifndef NAVIER_STOKES_HH
#define NAVIER_STOKES_HH

#include "data.hh"

inline void compute_navier_stokes(data* c, data* l, data* r, data* t, data* b, double h, vec2 g, double rho, double eta)
{
	// incompressible

	//vec2 d2vdxy = 0.5 * (t->dvdx - b->dvdx + r->dvdy - l->dvdy) / (2.0 * h);
	//vec2 grad_div_v = vec2(c->d2vdx2.x + d2vdxy.y, 
	//                       d2vdxy.x + c->d2vdy2.y);
	vec2 lap_v = c->d2vdx2 + c->d2vdy2;
	vec2 v_grad_v = vec2(c->v.x * c->dvdx.x + c->v.y * c->dvdy.x,
	                     c->v.x * c->dvdx.y + c->v.y * c->dvdy.y);
	
	// Navier-Stokes
	c->dvdt = g - (c->grad_p + eta * lap_v) / rho - v_grad_v;
	
	// dpdt = - div(p*v) = - (d/dx (p*v.x) + d/dy (p*v.y))
	//c->dpdt = - (c->grad_p.x * c->v.x + c->p * c->dvdx.x + c->grad_p.y * c->v.y + c->p * c->dvdy.y);
	
}

#endif


